package cs.utep.owlapitest;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Hello world!
 *
 */
public class GeoApp 
{
	 /**
     * @param args
     */
    public static void main( String[] args )
    {
    	String ontFile = "WeatherReportOntology.owl";

    	String prefix = "file:///";

    	URI basePhysicalURI = URI.create(prefix + ontFile.replace("\\", "/"));
    	OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    	//manager.makeLoadImportRequest(arg0);

    	try {
    		OWLOntology ontology = manager.createOntology(IRI.create(basePhysicalURI));
    		OWLDataFactory factory = manager.getOWLDataFactory();

    		//Adding Employee class
    		//What is an IRI? https://www.w3.org/TR/owl2-syntax/#IRIs
//    		OWLClass employeeClass = factory.getOWLClass(IRI.create(prefix + ontFile + "#Employee"));
//    		OWLAxiom employeeClassAxiom = factory.getOWLDeclarationAxiom(employeeClass);
//    		manager.addAxiom(ontology, employeeClassAxiom);
//    		
//    		OWLClass departmentClass = factory.getOWLClass(IRI.create(prefix + ontFile + "#Department"));
//    		OWLAxiom departmentClassAxiom = factory.getOWLDeclarationAxiom(departmentClass);
//    		manager.addAxiom(ontology, departmentClassAxiom);  
//    		
    		File file = new File(ontFile);
			file.createNewFile();
//
//			//Adding hasFirstName data property 
//			OWLDataProperty hasFirstNameProperty = factory.getOWLDataProperty(IRI.create(prefix + ontFile + "#hasFirstName"));
//
//			OWLAxiom hasFirstNamePropertyAxiom = factory.getOWLDataPropertyDomainAxiom(hasFirstNameProperty, employeeClass);
//			manager.applyChange(new AddAxiom(ontology,hasFirstNamePropertyAxiom));
//
//			//Adding hasLastName data property 
//			OWLDataProperty hasLastNameProperty = factory.getOWLDataProperty(IRI.create(prefix + ontFile + "#hasLastName"));
//			
//			OWLDatatype stringOwlDataType =  OWL2DatatypeImpl.getDatatype(OWL2Datatype.XSD_STRING);
//			OWLDataPropertyRangeAxiom hasNameRangeAxiom = factory.getOWLDataPropertyRangeAxiom(hasFirstNameProperty, stringOwlDataType);
//			manager.applyChange(new AddAxiom(ontology,hasNameRangeAxiom));			
//
//			OWLAxiom hasLastNamePropertyAxiom = factory.getOWLDataPropertyDomainAxiom(hasLastNameProperty, employeeClass);
//			manager.applyChange(new AddAxiom(ontology,hasLastNamePropertyAxiom));
//			
//			OWLDatatype stringlastOwlDataType =  OWL2DatatypeImpl.getDatatype(OWL2Datatype.XSD_STRING);
//			OWLDataPropertyRangeAxiom hasLastNameRangeAxiom = factory.getOWLDataPropertyRangeAxiom(hasLastNameProperty, stringlastOwlDataType);
//			manager.applyChange(new AddAxiom(ontology,hasLastNameRangeAxiom));			
//			
//			//Adding Object Property
//			OWLObjectProperty hasWeatherConditionObjectProperty = factory.getOWLObjectProperty(IRI.create(prefix + ontFile + "#hasWeatherCondition"));
//			OWLAxiom worksAtObjectPropertyDomainAxiom = factory.getOWLObjectPropertyDomainAxiom(worksAtObjectProperty, employeeClass);
//			manager.applyChange(new AddAxiom(ontology,worksAtObjectPropertyDomainAxiom));
//
//			OWLObjectPropertyRangeAxiom worksAtObjectPropertyRangeAxiom = factory.getOWLObjectPropertyRangeAxiom(worksAtObjectProperty, departmentClass);
//			manager.applyChange(new AddAxiom(ontology,worksAtObjectPropertyRangeAxiom));					
//			
//			//Employee
//			OWLNamedIndividual employeeIndividual = factory.getOWLNamedIndividual(IRI.create("Employee1"));
//			OWLClassAssertionAxiom employeeClassAssertion = factory.getOWLClassAssertionAxiom(employeeClass, employeeIndividual);
//			manager.applyChange(new AddAxiom(ontology,employeeClassAssertion));			
//			
//			OWLAxiom hasWeatherIndividualAxiom = factory.getOWLDataPropertyAssertionAxiom(hasFirstNameProperty, employeeIndividual, "Diego");
//			manager.applyChange(new AddAxiom(ontology, hasFirstNameIndividualAxiom));			
//
//			//OWLAxiom hasFirstNameIndividualAxiom = factory.getOWLDataPropertyAssertionAxiom(hasLastNameProperty, employeeIndividual, "Diego");
//			//manager.applyChange(new AddAxiom(ontology, hasFirstNameIndividualAxiom));	
//			
//			OWLNamedIndividual departmentIndividual = factory.getOWLNamedIndividual(IRI.create("Department1"));
//			OWLClassAssertionAxiom departmentClassAssertion = factory.getOWLClassAssertionAxiom(departmentClass, departmentIndividual);
//			manager.applyChange(new AddAxiom(ontology,departmentClassAssertion));				
//			
//			OWLAxiom worksAtAxiom = factory.getOWLObjectPropertyAssertionAxiom(worksAtObjectProperty, employeeIndividual, departmentIndividual);
//			manager.applyChange(new AddAxiom(ontology, worksAtAxiom));			

			String xmlURL = "http://w1.weather.gov/xml/current_obs/KLRU.xml"; // Can parse single location info
			/* Need to use url with lots of location data an example xml with 4 records is on Dropbox
			 * Inside folder: home/DigitalGeo/10 - Send2Git/WeatherReport.xml
			 * Dropbox url does not work even when set to public
			 * NEED TO EITHER USE local file path or get it uploaded in order to use as public url			 
			 */
			
    		//String xmlFile = "weather1.xml"; //[Trial file with records from 4 station_ids, who to incorporate this?]
    		//String xmlURL = URI.create(prefix + xmlFile.replace("\\", "/"));
			try {


				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(xmlURL);

				doc.getDocumentElement().normalize();

				System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

				NodeList nList = doc.getElementsByTagName("current_observation");

				System.out.println("----------------------------");

				for (int i = 0; i < nList.getLength(); i++) {

					Node nNode = nList.item(i);

					System.out.println("Observation tag #" + (i+1) + "\n");

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;
						//NEED TO GRAB FOLLOWING VALUES in a variable to be used instead of hard coding when passing into OWL
						System.out.println("Record : " + eElement.getAttribute("record_id"));	
						System.out.println("Location : " + eElement.getElementsByTagName("station_id").item(0).getTextContent());	
						System.out.println("longitude: " + eElement.getElementsByTagName("longitude").item(0).getTextContent());						
						System.out.println("weather: " + eElement.getElementsByTagName("weather").item(0).getTextContent());
						System.out.println("Temperature in F: " + eElement.getElementsByTagName("temp_f").item(0).getTextContent());
						System.out.println("Temperature in C: " + eElement.getElementsByTagName("temp_c").item(0).getTextContent());
						System.out.println("Relative Humidity: " + eElement.getElementsByTagName("relative_humidity").item(0).getTextContent());
						System.out.println("weather: " + eElement.getElementsByTagName("weather").item(0).getTextContent());
						//Example: for nested tags:
						//
						//Element eElement = (Element) nNode;

					}
				}

//				nList = doc.getElementsByTagName("department");
//
//				System.out.println("----------------------------");
//
//				for (int i = 0; i < nList.getLength(); i++) {
//
//					Node nNode = nList.item(i);
//
//					System.out.println("Department tag #" + (i+1) + "\n");
//
//					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//
//						Element eElement = (Element) nNode;
//
//						System.out.println("ID : " + eElement.getAttribute("id"));
//						System.out.println("Name : " + eElement.getElementsByTagName("name").item(0).getTextContent());
//
//					}
//				}
			} catch (Exception e) {
				e.printStackTrace();
			}			
			
			FileOutputStream outputStream = new FileOutputStream(file);
			manager.saveOntology(ontology, outputStream);
			System.out.println("Done Saving");    		

    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
